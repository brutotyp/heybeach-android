/*
 * Copyright (c) 2018. Daimler AG.
 */
package com.daimler.heybeach.android.model

data class BeachImageModel(var name: String, var url: String, var width: Int, var height: Int)
