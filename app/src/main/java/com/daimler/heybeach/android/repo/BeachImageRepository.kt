/*
 * Copyright (c) 2018. Daimler AG.
 */

package com.daimler.heybeach.android.repo

import android.os.AsyncTask
import android.util.Log
import com.daimler.heybeach.android.api.ApiRequest
import com.daimler.heybeach.android.api.Webservice
import com.daimler.heybeach.android.model.BeachImageModel
import org.json.JSONArray

class BeachImageRepository : Webservice {

    private lateinit var data: List<BeachImageModel>

    fun getBeaches(page: Int): String {
        return "/beaches?page=$page"
    }

    override fun fetch(request: ApiRequest): List<BeachImageModel> {
        try {
            val connection = Webservice.performRequest(request)

            val response = connection.inputStream.bufferedReader().readText()
            data = parseArrayResult(response)
        } catch (e: Exception) {
            Log.d("BeachImageRepository", "Error: $e")
        }

        return data
    }

    override fun parseArrayResult(response: String): List<BeachImageModel> {
        val jsonArray = JSONArray(response)
        val data = ArrayList<BeachImageModel>()

        for (item in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(item)
            val name = jsonObject.getString("name")
            val url = jsonObject.getString("url")
            val width = jsonObject.getInt("width")
            val height = jsonObject.getInt("height")
            val dataModel = BeachImageModel(name, url, width, height)
            data.add(dataModel)
        }

        return data
    }

    abstract class BeachImageAsyncTask : AsyncTask<String, String, List<BeachImageModel>>() {

        override fun doInBackground(vararg p0: String): List<BeachImageModel> {
            val repo = BeachImageRepository()
            return repo.fetch(Webservice.createGetRequest(p0[0]))
        }
    }
}
