/*
 * Copyright (c) 2018. Daimler AG.
 */

package com.daimler.heybeach.android.repo

import android.os.AsyncTask
import android.util.Log
import com.daimler.heybeach.android.api.ApiRequest
import com.daimler.heybeach.android.api.Webservice
import com.daimler.heybeach.android.model.UserModel
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection.HTTP_OK

class UserRepository : Webservice {

    private lateinit var data: UserModel

    fun getSignupPath(): String {
        return "/user/register"
    }

    fun getLoginPath(): String {
        return "/user/login"
    }

    fun getLogoutPath(): String {
        return "/user/logout"
    }

    fun getUserProfilePath(): String {
        return "/user/me"
    }

    override fun post(request: ApiRequest): UserModel {
        try {
            val connection = Webservice.performRequest(request)

            data = if (connection.responseCode == HTTP_OK) {
                val response = connection.inputStream.bufferedReader().readText()
                val token = connection.getHeaderField("x-auth")

                parseArrayResult(createJsonArrayString(response, token))
            } else {
                UserModel("", "")
            }
        } catch (e: Exception) {
            Log.d("UserRepo-FetchRequest", "Error: $e")
        }

        return data
    }

    override fun delete(request: ApiRequest): Int {
        var responseCode = 0

        try {
            val connection = Webservice.performRequest(request)

            responseCode = connection.responseCode
        } catch (e: Exception) {
            Log.d("UserRepo-DeleteRequest", "Error: $e")
        }

        return responseCode
    }

    override fun fetch(request: ApiRequest): UserModel {
        try {
            val connection = Webservice.performRequest(request)

            if (connection.responseCode == HTTP_OK) {
                val response = connection.inputStream.bufferedReader().readText()
                data = parseObjectResult(response)
            }
        } catch (e: Exception) {
            Log.d("UserRepo-GetRequest", "Error: $e")
        }

        return data
    }


    override fun parseArrayResult(response: String): UserModel {
        val jsonArray = JSONArray(response)

        return UserModel(
                jsonArray.getJSONObject(0).getString("email"),
                jsonArray.getJSONObject(1).getString("token"))
    }

    override fun parseObjectResult(response: String): UserModel {
        val jsonObject = JSONObject(response)

        return UserModel(jsonObject.getString("email"), "")
    }

    /**
     * This is more of a hacky solution to scrape the JWT token out of the response header
     * and to make it available for the rest of the application via the user model
     * Definitely worth to refactor this sometime....but right now, it works :P
     * @param response String the response body, in fact the real content
     * @param token String the token scraped out of the x-auth header
     * @return String a pretty nice json array string
     */
    fun createJsonArrayString(response: String, token: String): String {
        return "[$response, {\"token\":\"$token\"}]"
    }

    abstract class LoginSignupTask : AsyncTask<String, String, UserModel>() {

        override fun doInBackground(vararg p0: String): UserModel {
            val repo = UserRepository()
            return repo.post(Webservice.createPostRequest(p0[0], p0[1]))
        }
    }

    abstract class LogoutTask : AsyncTask<String, String, Int>() {

        override fun doInBackground(vararg p0: String): Int {
            val repo = UserRepository()
            return repo.delete(Webservice.createDeleteRequest(p0[0], p0[1]))
        }
    }

    abstract class UpdateUserProfileTask : AsyncTask<String, String, UserModel>() {

        override fun doInBackground(vararg p0: String): UserModel {
            val repo = UserRepository()
            return repo.fetch(Webservice.createGetRequest(p0[0], p0[1]))
        }
    }
}
