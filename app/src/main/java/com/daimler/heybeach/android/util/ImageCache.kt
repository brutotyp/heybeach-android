/*
 * Copyright (c) 2018. Daimler AG.
 */

package com.daimler.heybeach.android.util

import android.graphics.Bitmap
import android.util.LruCache

/**
 * We want to have one global image cache for the whole application, so we setup a singleton for the
 * image cache
 */
object ImageCache {

    /**
     * We could have used a simple HashMap here, but the LruCache is better since we can define a
     * maxCacheSize. If the cache exceeds old elements get thrown out and can be garbage collected
     */
    private val imageCache: LruCache<String, Bitmap> = LruCache(defaultImageCacheSize)

    /**
     * This method is a wrapper for storing new images in the cache
     * @param key String anything unique for identification
     * @param image Bitmap the image itself as Bitmap representation
     */
    fun store(key: String, image: Bitmap) {
        imageCache.put(key, image)
    }

    /**
     * This method is a wrapper for reading stored images from the cache
     * @param key String the unique identifier of a stored image
     * @return Bitmap? result can be null, if no image is found
     */
    fun read(key: String): Bitmap? {
        return imageCache[key]
    }

    /**
     * Depending on the device the jvm has a defined amount of available memory
     * We use for the image cache around 12% of this memory (2GB Memory -> 256MB Cache)
     */
    private val defaultImageCacheSize: Int
        get() {
            val maxMemory = (Runtime.getRuntime().maxMemory() / 1024).toInt()
            return maxMemory / 8
        }
}
