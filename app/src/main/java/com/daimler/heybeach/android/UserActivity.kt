/*
 * Copyright (c) 2018. Daimler AG.
 */

package com.daimler.heybeach.android

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.daimler.heybeach.android.model.UserModel
import com.daimler.heybeach.android.repo.UserRepository
import com.daimler.heybeach.android.util.Prefs
import kotlinx.android.synthetic.main.activity_user.*

class UserActivity : AppCompatActivity() {

    private var prefs: Prefs? = null
    private var userRepo: UserRepository? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        // Get instance of shared prefs
        prefs = Prefs(this)
        // Get instance of the user repo for profile functionality
        userRepo = UserRepository()

        // Get the JWT token from shared prefs
        val token = prefs!!.jwtToken
        UpdateUserProfile().execute(userRepo!!.getUserProfilePath(), token)
    }

    inner class UpdateUserProfile : UserRepository.UpdateUserProfileTask() {

        override fun onPostExecute(result: UserModel) {
            super.onPostExecute(result)

            // Just get the user name (first part of mail address)
            name.text = result.email.substring(0, result.email.indexOf('@'))
            // also set the mail address
            email.text = result.email
        }
    }
}
