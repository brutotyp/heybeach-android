/*
 * Copyright (c) 2018. Daimler AG.
 */

package com.daimler.heybeach.android

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.daimler.heybeach.android.model.UserModel
import com.daimler.heybeach.android.repo.UserRepository
import com.daimler.heybeach.android.util.JsonHelper
import com.daimler.heybeach.android.util.Prefs
import kotlinx.android.synthetic.main.activity_login.*

/*
 * Define constants
 */
private const val REQUEST_SIGNUP = 0

class LoginActivity : AppCompatActivity() {

    private var prefs: Prefs? = null
    private var userRepo: UserRepository? = null

    lateinit var email: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // Get instance of shared prefs
        prefs = Prefs(this)
        // Get instance of the user repo for login functionality
        userRepo = UserRepository()

        // Check if there is a JWT token available
        checkForJwtToken()

        btn_login.setOnClickListener({
            // perform the login
            login()
        })

        link_signup.setOnClickListener({
            // start the signup activity
            val intent = Intent(applicationContext, SignupActivity::class.java)
            startActivityForResult(intent, REQUEST_SIGNUP)
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        })
    }

    private fun checkForJwtToken() {
        val token = prefs!!.jwtToken

        // if there is a token available, the user does not need to login manually
        if (!token.isEmpty()) {
            onLoginSuccess()
        }
    }

    private fun login() {
        if (!validate()) {
            onLoginFailed()
            return
        }

        btn_login.isEnabled = false
        progress_bar.visibility = View.VISIBLE

        email = input_email.text.toString()
        val password = input_password.text.toString()

        val loginBody = JsonHelper.createJsonObjectString(email, password)

        PerformLoginTask().execute(userRepo!!.getLoginPath(), loginBody)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == Activity.RESULT_OK) {
                // After a successful sign up the user is logged in directly
                onLoginSuccess()
            }
        }
    }

    fun onLoginSuccess() {
        btn_login.isEnabled = true

        // After successful login procedure start the beach image activity
        val intent = Intent(this@LoginActivity, BeachImageActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)

        finish()
    }

    fun onLoginFailed() {
        Toast.makeText(applicationContext, "Login failed", Toast.LENGTH_LONG).show()

        btn_login.isEnabled = true
    }

    private fun validate(): Boolean {
        var valid = true

        val email = input_email.text.toString()
        val password = input_password.text.toString()

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            input_email.error = "enter a valid email address"
            valid = false
        } else {
            input_email.error = null
        }

        if (password.isEmpty() || password.length < 6) {
            input_password.error = "at least 6 characters"
            valid = false
        } else {
            input_password.error = null
        }

        return valid
    }

    override fun onBackPressed() {
        // disable hw/sw back button on login screen
        moveTaskToBack(true)
    }

    inner class PerformLoginTask : UserRepository.LoginSignupTask() {

        override fun onPostExecute(result: UserModel) {
            super.onPostExecute(result)

            progress_bar.visibility = View.INVISIBLE

            // Test if the entered mail address is available in our response from the server
            // if yes, we can be pretty sure, that the user is registered and is allowed to go further
            // hence, we can log him in
            if (result.email == email) {
                // save the new token to the shared prefs
                prefs!!.jwtToken = result.token
                onLoginSuccess()
            } else {
                onLoginFailed()
            }
        }
    }
}
