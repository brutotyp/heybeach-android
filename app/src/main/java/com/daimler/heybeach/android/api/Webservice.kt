/*
 * Copyright (c) 2018. Daimler AG.
 */

package com.daimler.heybeach.android.api

import android.util.Log
import java.io.BufferedOutputStream
import java.net.HttpURLConnection
import java.net.URL

interface Webservice {

    companion object {

        const val BASE_URL = "http://techtest.lab1886.io:3000"

        /**
         *
         * @param path String
         * @param token String
         * @return ApiRequest
         */
        fun createGetRequest(path: String, token: String = ""): ApiRequest {
            val method = "GET"
            val header: Array<Pair<String, String>> = if (token.isEmpty()) {
                arrayOf(Pair("Cache-Control", "no-cache"))
            } else {
                arrayOf(Pair("x-auth", token), Pair("Cache-Control", "no-cache"))
            }

            return ApiRequest(method, header, "", createRequestURL(path))
        }

        /**
         *
         * @param path String
         * @param body String
         * @return ApiRequest
         */
        fun createPostRequest(path: String, body: String): ApiRequest {
            val method = "POST"
            val header = arrayOf(Pair("Content-Type", "application/json"), Pair("Cache-Control", "no-cache"))

            return ApiRequest(method, header, body, createRequestURL(path))
        }

        /**
         *
         * @param path String
         * @param token String
         * @return ApiRequest
         */
        fun createDeleteRequest(path: String, token: String): ApiRequest {
            val method = "DELETE"
            val header = arrayOf(Pair("x-auth", token), Pair("Cache-Control", "no-cache"))

            return ApiRequest(method, header, "", createRequestURL(path))
        }

        fun performRequest(request: ApiRequest): HttpURLConnection {
            // Open the connection with the given url
            val connection = request.url.openConnection() as HttpURLConnection

            // Set the connection method
            connection.requestMethod = request.method

            // Add all respective request headers to the connection
            for (header in request.header) {
                connection.setRequestProperty(header.first, header.second)
            }

            // Do the post request
            if (connection.requestMethod == "POST") {
                try {
                    connection.doOutput = true

                    if (!request.body.isEmpty()) {
                        val out = BufferedOutputStream(connection.outputStream)
                        out.write(request.body.toByteArray())
                        out.flush()
                        out.close()
                    }
                } catch (e: Exception) {
                    Log.e("SendPostRequest", "Error: $e")
                }
            }

            return connection
        }

        /**
         *
         * @param path String
         * @return URL
         */
        private fun createRequestURL(path: String): URL {
            return URL("$BASE_URL$path")
        }
    }

    /**
     *
     * @param request ApiRequest
     * @return Any
     */
    fun fetch(request: ApiRequest): Any {
        return Any()
    }

    /**
     *
     * @param request ApiRequest
     * @return Any
     */
    fun post(request: ApiRequest): Any {
        return Any()
    }

    /**
     *
     * @param request ApiRequest
     * @return Any
     */
    fun delete(request: ApiRequest): Any {
        return Any()
    }

    /**
     *
     * @param response String
     * @return List<Any>
     */
    fun parseArrayResult(response: String): Any {
        return Any()
    }

    /**
     *
     * @param response String
     * @return Any
     */
    fun parseObjectResult(response: String): Any {
        return Any()
    }
}
