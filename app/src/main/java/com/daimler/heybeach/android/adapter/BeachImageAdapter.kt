/*
 * Copyright (c) 2018. Daimler AG.
 */

package com.daimler.heybeach.android.adapter

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.daimler.heybeach.android.R
import com.daimler.heybeach.android.api.Webservice
import com.daimler.heybeach.android.model.BeachImageModel
import com.daimler.heybeach.android.util.ImageCache
import kotlinx.android.synthetic.main.beach_list_item.view.*
import java.io.IOException
import java.lang.ref.WeakReference
import java.net.HttpURLConnection
import java.net.URI
import java.net.URL

class BeachImageAdapter(private val context: Context) : RecyclerView.Adapter<BeachImageAdapter.BeachImageViewHolder>() {

    private var imageList: MutableList<BeachImageModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeachImageViewHolder {
        return BeachImageViewHolder(LayoutInflater.from(context).inflate(R.layout.beach_list_item, parent, false))
    }

    override fun getItemCount(): Int = imageList.size

    override fun onBindViewHolder(holder: BeachImageViewHolder, position: Int) {
        // We may get into race conditions on fast devices, so the bind can be a bit more faster
        // than data has been published in the image list
        if (imageList.isEmpty()) {
            return
        }

        val imageItem = imageList[position]

        // Attempt to load image from cache
        val image = ImageCache.read(imageItem.url)

        if (image == null) {
            // Image was not found cache -> make an api call
            val imageUrl = "${Webservice.BASE_URL}/${imageItem.url}"
            GetImageFor(holder.imageView).execute(imageUrl)
        } else {
            // Render image directly
            holder.imageView.setImageBitmap(image)
        }

        // Set beach image title
        holder.textView.text = imageItem.name
    }

    class BeachImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView = itemView.beach_image!!
        var textView = itemView.beach_image_title!!
    }

    inner class GetImageFor(imageView: ImageView) : AsyncTask<String, Void, Bitmap>() {
        private var img: WeakReference<ImageView> = WeakReference(imageView)

        override fun doInBackground(vararg p0: String): Bitmap? {
            return getBitmapFromUrl(p0[0])
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            if (result != null) {
                // Render image in the given image view
                img.get()?.setImageBitmap(result)
            }
        }

        private fun getBitmapFromUrl(url: String): Bitmap? {
            var bitmap: Bitmap? = null

            try {
                val connection = URL(url).openConnection() as HttpURLConnection
                bitmap = BitmapFactory.decodeStream(connection.inputStream)
            } catch (e: IOException) {
                Log.d("BitmapParser", "Error: $e")
            }

            // Store image in global cache
            if (bitmap != null) {
                // Take only the path of the image url and drop the leading "/", so that it stores
                // the correct string as provided by api
                ImageCache.store(URI(url).path.drop(1), bitmap)
            }

            return bitmap
        }
    }

    /**
     * Helper
     */
    fun addImages(result: List<BeachImageModel>) {
        for (image in result) {
            imageList.add(image)
            // This is a very important step:
            // Since we have a mutable list which is changed during async tasks and reloading data
            // in recycler view we have to actively notify the the list size has been changed
            notifyDataSetChanged()
        }
    }
}
