/*
 * Copyright (c) 2018. Daimler AG.
 */

package com.daimler.heybeach.android

import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.daimler.heybeach.android.adapter.BeachImageAdapter
import com.daimler.heybeach.android.adapter.PaginationScrollListener
import com.daimler.heybeach.android.model.BeachImageModel
import com.daimler.heybeach.android.repo.BeachImageRepository
import com.daimler.heybeach.android.repo.UserRepository
import com.daimler.heybeach.android.util.Prefs
import kotlinx.android.synthetic.main.activity_beach_image.*
import java.net.HttpURLConnection.HTTP_OK

/*
 * Define constants
 */
private const val PAGE_START = 0
private const val TOTAL_PAGES = 5

class BeachImageActivity : AppCompatActivity() {

    private var prefs: Prefs? = null
    var adapter: BeachImageAdapter? = null
    private var beachRepo: BeachImageRepository? = null
    private var userRepo: UserRepository? = null

    private var currentPage: Int = PAGE_START
    private var isLastPage: Boolean = false
    private var isLoading: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beach_image)

        // Get instance of shared prefs
        prefs = Prefs(this)
        // Get instance of the recycler view adapter -> the explicit context here is very important!
        // otherwise the async task will have different context
        adapter = BeachImageAdapter(this@BeachImageActivity)
        // Get instance of the repo for this activity
        beachRepo = BeachImageRepository()
        // Get instance of the user repo for logout functionality
        userRepo = UserRepository()

        // Define the layout manager and attach it to the recycler view
        // For Pinterest like style, we use the staggered grid
        val layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        beach_list.layoutManager = layoutManager
        beach_list.itemAnimator = DefaultItemAnimator()
        beach_list.setItemViewCacheSize(18)
        // Attach the recycler view adapter
        beach_list.adapter = adapter

        // We want to load new images from the paginated api, when the view is scrolled to the end
        // of the recent set of images. We use a customized scroll listener.
        val scrollListener = object : PaginationScrollListener(layoutManager) {
            override fun loadMoreItems() {
                currentPage += 1
                loadPage()
            }

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }
        }

        // Attach the newly defined scroll listener to the recycler view
        beach_list.addOnScrollListener(scrollListener)

        // At the end of activity creation finally load the first image set
        loadPage()
    }

    private fun loadPage() {
        // Right now we start async loading data from web api
        isLoading = true

        UpdateBeachImages().execute(beachRepo!!.getBeaches(currentPage))

        if (currentPage >= TOTAL_PAGES) {
            isLastPage = true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_logout -> {
                logout()
                return true
            }
            R.id.action_me -> {
                showUserProfile()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun logout() {
        // OPTIONAL: Show "Are you sure?" dialog
        // Get the JWT token from shared prefs
        val token = prefs!!.jwtToken
        PerformLogoutTask().execute(userRepo!!.getLogoutPath(), token)
    }

    fun onLogoutSuccess() {
        // Delete the JWT token from the shared prefs
        prefs!!.jwtToken = ""

        // Get the user back to the login screen
        val intent = Intent(applicationContext, LoginActivity::class.java)
        intent.flags = (Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down)

        finish()
    }

    fun onLogoutFailed() {
        Toast.makeText(applicationContext, "Logout failed", Toast.LENGTH_LONG).show()
    }

    private fun showUserProfile() {
        val userIntent = Intent(this@BeachImageActivity, UserActivity::class.java)

        val pendingIntent = TaskStackBuilder.create(this)
                // This uses android:parentActivityName in AndroidManifest
                .addNextIntentWithParentStack(userIntent)
                .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)

        pendingIntent.send()

        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
    }

    inner class UpdateBeachImages : BeachImageRepository.BeachImageAsyncTask() {

        override fun onPostExecute(result: List<BeachImageModel>) {
            super.onPostExecute(result)

            // When the async task is done add the new images from the result to the image list
            // in the recycler view adapter
            adapter!!.addImages(result)
            // Now the blocking indicator for loading async data can be reset
            isLoading = false
        }
    }

    inner class PerformLogoutTask : UserRepository.LogoutTask() {

        override fun onPostExecute(result: Int) {
            super.onPostExecute(result)

            if (result == HTTP_OK) {
                onLogoutSuccess()
            } else {
                onLogoutFailed()
            }
        }
    }
}
