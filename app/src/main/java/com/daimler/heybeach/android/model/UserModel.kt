/*
 * Copyright (c) 2018. Daimler AG.
 */

package com.daimler.heybeach.android.model

data class UserModel(val email: String, val token: String)
