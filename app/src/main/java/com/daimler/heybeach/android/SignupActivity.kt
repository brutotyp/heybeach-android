/*
 * Copyright (c) 2018. Daimler AG.
 */

package com.daimler.heybeach.android

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.daimler.heybeach.android.model.UserModel
import com.daimler.heybeach.android.repo.UserRepository
import com.daimler.heybeach.android.util.JsonHelper
import com.daimler.heybeach.android.util.Prefs
import kotlinx.android.synthetic.main.activity_signup.*


class SignupActivity : AppCompatActivity() {

    private var prefs: Prefs? = null
    private var userRepo: UserRepository? = null

    lateinit var email: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        // Get instance of shared prefs
        prefs = Prefs(this)
        // Get instance of the user repo for signup functionality
        userRepo = UserRepository()

        btn_signup.setOnClickListener({
            // perform the signup
            signup()
        })

        link_login.setOnClickListener({
            // get back to the calling login activity
            finish()
            overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down)
        })
    }

    fun signup() {
        if (!validate()) {
            onSignupFailed()
            return
        }

        btn_signup.isEnabled = false
        progress_bar.visibility = View.VISIBLE

        email = input_email.text.toString()
        val password = input_password.text.toString()

        val signupBody = JsonHelper.createJsonObjectString(email, password)

        PerformSignupTask().execute(userRepo!!.getSignupPath(), signupBody)
    }

    fun onSignupSuccess() {
        btn_signup.isEnabled = true
        setResult(Activity.RESULT_OK, null)
        finish()
    }

    fun onSignupFailed() {
        Toast.makeText(applicationContext, "Signup failed", Toast.LENGTH_LONG).show()

        btn_signup.isEnabled = true
    }

    private fun validate(): Boolean {
        var valid = true

        val email = input_email.text.toString()
        val password = input_password.text.toString()
        val passwordRepeat = input_password_repeat.text.toString()

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            input_email.error = "enter a valid email address"
            valid = false
        } else {
            input_email.error = null
        }

        if (password.isEmpty() || password.length < 6) {
            input_password.error = "at least 6 characters"
            valid = false
        } else {
            input_password.error = null
        }

        if (passwordRepeat.isEmpty() || passwordRepeat != password) {
            input_password_repeat.error = "passwords do not match"
            valid = false
        } else {
            input_password_repeat.error = null
        }

        return valid
    }

    inner class PerformSignupTask : UserRepository.LoginSignupTask() {

        override fun onPostExecute(result: UserModel) {
            super.onPostExecute(result)

            progress_bar.visibility = View.INVISIBLE

            // Test if the entered mail address is available in our response from the server
            // if yes, we can be pretty sure, that the user is registered and is allowed to go further
            // hence, we can log him in
            if (result.email == email) {
                // save the new token to the shared prefs
                prefs!!.jwtToken = result.token
                onSignupSuccess()
            } else {
                onSignupFailed()
            }
        }
    }
}
