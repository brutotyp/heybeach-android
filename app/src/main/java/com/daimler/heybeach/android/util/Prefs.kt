/*
 * Copyright (c) 2018. Daimler AG.
 */

package com.daimler.heybeach.android.util

import android.content.Context
import android.content.SharedPreferences

class Prefs(context: Context) {
    private val PREFS_FILENAME = "com.daimler.heybeach.android.prefs"
    private val JWT_TOKEN = "jwt_token"
    private val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0)

    var jwtToken: String
        get() = prefs.getString(JWT_TOKEN, "")
        set(value) = prefs.edit().putString(JWT_TOKEN, value).apply()
}
