/*
 * Copyright (c) 2018. Daimler AG.
 */

package com.daimler.heybeach.android.adapter

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager

abstract class PaginationScrollListener(var layoutManager: StaggeredGridLayoutManager) : RecyclerView.OnScrollListener() {

    private val threshold = 3

    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        // Just a little helper variable -> the first value of firstVisibleItems array is always
        // the count of past visible items
        var pastVisibleItems = -1

        var firstVisibleItems: IntArray? = null
        firstVisibleItems = layoutManager.findFirstVisibleItemPositions(firstVisibleItems)

        if (firstVisibleItems.isNotEmpty()) {
            pastVisibleItems = firstVisibleItems[0]
        }

        // this is the _crucial_ part of decision making when to reload new data form the api
        // First check if already async task loading is in progress or the last page of the api
        // was reached
        if (!isLoading() && !isLastPage()) {
            // Only load more item if we scrolled down to the end of the list of items and all
            // items already loaded are visible or have been seen
            if ((visibleItemCount + pastVisibleItems) >= totalItemCount - threshold) {
                loadMoreItems()
            }
        }
    }

    abstract fun loadMoreItems()

    abstract fun isLastPage(): Boolean

    abstract fun isLoading(): Boolean
}
