/*
 * Copyright (c) 2018. Daimler AG.
 */

package com.daimler.heybeach.android.api

import java.util.*
import java.net.URL

data class ApiRequest(val method: String, val header: Array<Pair<String, String>>, val body: String, val url: URL) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ApiRequest

        if (method != other.method) return false
        if (!Arrays.equals(header, other.header)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = method.hashCode()
        result = 31 * result + Arrays.hashCode(header)
        return result
    }
}
