# [HeyBeach](https://bitbucket.org/brutotyp/heybeach-android)

HeyBeach is a beautiful designed application for the Android platform, which brings you a nice curated list of the best beaches in the world. You need to login or register with your email and a password at first and afterwards you will be facing a pinterest style like view of beach images which are loaded infinite while scrolling. Since it is very difficult for our editors to pic only the best images for you, you will be facing 30 images at the end. A nice gimmick is the small but nice User Profile, where you can see all your saved data (since GDPR update very important! We take care of your personal data!) Stay tuned for more to come!

## Authors

### Kevin Faust

+ [Website](https://kevinfaust.de)
+ [Github](https://github.com/mrhahn3000)

## Getting Started

1. Clone the Repo  
`git clone https://bitbucket.org/brutotyp/heybeach-android.git`

2. Build with Gradle and Kotlin  
Open Android Studio and hit `run`.

3. Choose your device  
Either choose a real device or go with built in Android emulators.

4. There is no step 4  
Enjoy to see your running app.

## File Structure

```plaintext
root
└── src
    └── android
        ├── adapter
        │   ├── BeachImageAdapter.kt
        │   └── PaginationScrollListener.kt
        ├── api
        │   ├── ApiRequest.kt
        │   └── Webservice.kt
        ├── model
        │   ├── BeachImageModel.kt
        │   └── UserModel.kt
        ├── repo
        │   ├── BeachImageRepository.kt
        │   └── UserRepository.kt
        ├── util
        │   ├── ImageCache.kt
        │   ├── JsonHelper.kt
        │   └── Prefs.kt
        ├── BeachImageActivity.kt
        ├── LoginActivity.kt
        ├── SignupActivity.kt
        └── UserActivity.kt
```

## Testing

Manual Testing has been done throughout the development period. However, it definitly needs to be done more! Implementing Unit and Integration Tests is very important. However, writing good tests which do not simply fulfill a code coverage is quite time consuming. So I was faced during this coding task to either write sophisticated tests or implement the required business logic for the required/prioritized features. Guess what I did ;-)

## Roadmap

Done is better than perfect! Every piece of software can and has to be improved day by day (or sprint by sprint, whatever you might prefer). During this rough phase of development I was faced with a lot of improvements, new features and so on, what I wanted to implement, but unfortunately time flew by. So here is a little roadmap (you can also call it 'open issues').

### v1.1.0

[ ]  Add CI  
[ ]  Testing! Implement unit and integration tests  
[ ]  Add more user-facing feedback (loading indicators, ...)  
[ ]  Improve the software architecture with MVVM  
[ ]  Improve Error and Exception Handling  
[ ]  More shiny animations  
[ ]  ...  